{
  communities {
    name
    openness
    profile {
      id
      uri
      summary
      icon
    }
    memberOf {
      name
      profile {
        uri
        icon
      }
    }
    comments {
      content
      replies {
        content
        activity {
          inReplyTo {
            activity {
              uri
            }
          }
        }
      }
    }
    collections {
      name
      learningInfo {
        resourceTypes
        levels {
          framework
          name
          uri
        }
        subjects {
          framework
          name
          path
          uri
        }
        ageRange
        audiences
        uses
        timeRequired
      }
      resources {
        resource {
          uri
          summary
          tags {
            name
            path
            type
          }
          icon
          learningInfo {
            resourceTypes
            levels {
              framework
              name
              uri
            }
            subjects {
              framework
              name
              path
              uri
            }
            ageRange
            audiences
            uses
            timeRequired
          }
          primaryLanguage
          languages
          likeCount
        }
        comments {
          content
        }
      }
    }
  }
}
